unit LogUtils;

interface

uses
  Winapi.Windows, System.SysUtils;

procedure Log(Mensagem: string);
procedure Cliente(Mensagem: string);

implementation

var
  LastLog: Cardinal = 0;
  Loging : Boolean = False;

procedure Cliente(Mensagem: string);
var
  nome: string;
  exibir: TextFile;
begin

  nome := 'Clientes.log';
  AssignFile(exibir, nome);

  if FileExists(nome) then
    Append(exibir)
  else
    Rewrite(exibir);

  try
    WriteLn(exibir, Mensagem);
  finally
    CloseFile(exibir);
  end;
end;

procedure Log(Mensagem: string);
var
  n: string;
  h: TextFile;
  p_1, p_2: Cardinal;
begin
  p_1 := GetTickCount;

  if LastLog <> 0 then
    p_2 := p_1 - LastLog
  else
    p_2 := 0;

  LastLog := p_1;
  n := ChangeFileExt(ParamStr(0), '.log');
  AssignFile(h, n);

  if FileExists(n) then
    Append(h)
  else
    Rewrite(h);

  try
    WriteLn(h, DateTimeToStr(Now) + ' (' + IntToStr(p_2) + 'ms): ' + Mensagem);
  finally
    CloseFile(h);
  end;
end;

end.
