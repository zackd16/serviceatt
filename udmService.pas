unit udmService;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TdmService = class(TService)
    fdConn: TFDConnection;
    dsDados: TDataSource;
    qrDados: TFDQuery;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceAfterUninstall(Sender: TService);
    procedure ServiceBeforeInstall(Sender: TService);
    procedure ServiceBeforeUninstall(Sender: TService);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  dmService: TdmService;

implementation

{$R *.dfm}
uses
  LogUtils;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  dmService.Controller(CtrlCode);
end;

function TdmService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TdmService.ServiceAfterInstall(Sender: TService);
begin
  Log('Servi�o p�s-instala��o');
end;

procedure TdmService.ServiceAfterUninstall(Sender: TService);
begin
  Log('Servi�o p�s-desinstala��o');
end;

procedure TdmService.ServiceBeforeInstall(Sender: TService);
begin
  Log('Servi�o pr�-instala��o');
end;

procedure TdmService.ServiceBeforeUninstall(Sender: TService);
begin
  Log('Servi�o pr�-desinstala��o');
end;

procedure TdmService.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
  Log('Servi�o retornado');
  Continued := true;
end;

procedure TdmService.ServiceExecute(Sender: TService);
begin
  qrDados.Open();

  Log('Servi�o executado');
  Cliente(qrDados.FieldByName('Cliente_nome').AsString);

   while not Self.Terminated do
   begin
     Sleep(200);
     ServiceThread.ProcessRequests(True);
   end;
end;

procedure TdmService.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Log('Servi�o pausado');
  Paused := true;
end;

procedure TdmService.ServiceShutdown(Sender: TService);
begin
  Log('Shutdown');
end;

procedure TdmService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Log('Servi�o iniciado');
  Started := true;
end;

procedure TdmService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Log('Servi�o finalizado');
  Stopped := true;
end;

end.
